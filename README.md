# module-boilerplate

Find and replace 'module-boilerplate' to 'your-module'.


### Install

```shell
composer config repositories.bizcommerce-theme-conexia git https://bitbucket.org/biz-commerce/theme-conexia.git
composer require biz-commerce/theme-conexia:dev-master
php bin/magento setup:static-content:deploy pt_BR
php bin/magento setup:upgrade
php bin/magento setup:di:compile
```

### Test
```shell
make test
```