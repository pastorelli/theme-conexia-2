FROM iad.ocir.io/biz/biz-commerce/biz-module
ARG DRONE_REPO

RUN echo $DRONE_REPO
RUN echo ${DRONE_REPO##*/}

RUN composer config repositories.bizcommerce-${DRONE_REPO##*/} git https://bitbucket.org/biz-commerce/${DRONE_REPO##*/}.git
RUN composer require biz-commerce/${DRONE_REPO##*/}:dev-master